# Overview

This repository contains the Visual Studio project files for the data processing library to be implemented by the _Comms_ subsystem.

- `ENGG_T3CommsB_DP` contains the source and header files constituting the library.
- `ENGG_T3CommsB_DP_Tests` contains testing files, implementing the Google Test library. A copy of the Google Test library is included in `packages`.

# Getting started

If you responsible for the client-to-server communications of the _Comms_ subsystem, please refer to both the _implementation_ and _interface_ guides below. Otherwise, you will only need to refer to the _interface_ guide. Please refer to the _documentation_ section for a more complete description of functionality. These guides are only intended to outline as briefly as possible the fundamentals of how to interact with the library.

## Implementation

1. Import the library with `#import "RQ.h"`.
2. In the main Arduino sketch, declare an `RQ` (_request queue_) instance and instantiate it in the `setup()` method.

## Interfacing

1. Import the library with `#import "RQ.h"`.
2. Define a class extending `RQO` (_request queue observer_).
3. Override the `notify(Req *)` method.
4. Instantiate an instance of the `RQO` subclass.
5. Invoke `subscribe(RQO *)` on the `RQ` (_request queue_) instance you intend to interact with, passing a reference to `RQO` subclass instance as the parameter.

# Documentation

## Req.h

### Product

An enumeration of `unsigned char` representing the products (i.e. cans). The `unsigned char` that each product corresponds to is the _product ID_ (referred to as `pID` throughout the source code) of the product.

- `RED = 0`: Red can.
- `GREEN = 1`: Green can.
- `BLUE = 2`: Blue can.
- `NUM_PRODS = 3`: Used throughout the source code to specify the number of products. A product ID should be _strictly less than_ this value.

### Status

An enumeration of `unsigned char` representing request _statuses_. Each `Req` has a _status_ field. There are five predefined statuses for values `0` to `4`. Other values may be designated for statuses (particularly errors) whose specification is outside the scope of this library. See documentation for the `Req` and `RQ` classes for further information on how statuses are interpreted internally.

- `WRITE = 0`: The default status, indicating that the `Req` has not yet been submitted to the queue.
- `WAIT = 1`: Indicates that the `Req` is in the queue and awaiting processing.
- `ACTIVE = 2`: Indicates that the `Req` is the head of the queue and is currently under delivery by the system.
- `SUCCESS = 3`: Indicates that the `Req` has successfully been delivered.
- `CANCELLED = 4`: Indicates that the `Req` has been cancelled by the user.

Statuses other than `WRITE`, `WAIT`, and `ACTIVE` are considered _terminal_ statuses. Requests with these statuses are subject to deletion (i.e. removed from memory) once they reach the top of the queue. This includes all externally defined statuses.

### Req

Represents a _request_, comprised of product-quantity pairs.

Index of fields and methods:

- `Req()`
- `setQuant(unsigned char pID, unsigned char quant) : void`
- `getID() : unsigned char`
- `getStatus() : unsigned char`
- `getQuant(unsigned char pID) : unsigned char`

The following fields and methods should **not** be used externally:

- `~Req()`
- `setStatus(unsigned char status) : void`

Status changes to a request should be undertaken via the `setStatus` method in `RQ`, not in `Req`.

### Req (constructor)

`Req::Req()`

Instantiates a new empty request with status `WRITE`. The new instance is assigned an ID (unique within a session).

### setQuant

`void Req::setQuant(unsigned char pID, unsigned char quant)`

Sets the quantity of product `pID` to `quant` in the request.

This method will exit immediately if any of the following misuses are detected:

- `pID` is greater than or equal to `NUM_PRODS`,
- The status of the request is not `WRITE`.

### getID

`unsigned char Req::getID()`

Returns the ID of the request. Request IDs are unique within a single session.

### getStatus

`unsigned char Req::getStatus()`

Returns the status of the request.

### getQuant

`unsigned char Req::getQuant(unsigned char pID)`

Returns the quantity of product `pID` specified by this request.

This method will exit immediately if `pID` is greater than or equal to `NUM_PRODS` (i.e. it refers to a product that does not exist), returning `0`.

## RQ.h

### Constants

- `MAX_REQS = 16`: The maximum number of requests the queue may contain.
- `MAX_RQOS = 16`: The maximum number of `RQO` instances that may subscribe to the queue.

### RQO

An `RQO` is a _request queue observer_. It has a single interface method `virtual void notify(Req *req)`. Whenever the status of a request in the queue is modified, all `RQO` instances subscribed to the queue have `notify(req)` invoked on them, with `req` a reference to the request whose status was modified.

An overriding of `notify` should constitute checking the request for its status (such as via a `switch` block over `req->getStatus()`, or a series of conditional blocks), taking appropriate action based on its status.

**Caution**: Requests with terminal statuses are _subject for deletion_. For this reason, unless the status is non-terminal, all necessary data from the `Req` instance should be obtained within this method, and the reference to the request should _not_ should stored as a variable or accessed outside the scope of the method.

### RQ

The _request queue_. A single `RQ` instance should be instantiated for a single session.

Index of fields and methods:

- `RQ()`
- `subscribe(RQO *rqo) : void`
- `add(Req *req) : void`
- `setStatus(Req *req, unsigned char status) : void`
- `setStatus(unsigned char rID, unsigned char status) : void`
- `get() : Req *`
- `get(unsigned char rID) : Req *`

The following fields and methods should **not** be used externally:

- `~RQ()`

### RQ (constructor)

Instantiates a new empty request queue.

### subscribe

`void RQ::subscribe(RQO *rqo)`

Subscribes the referenced request queue observer to the queue. Once subscribed, the `notify` method of the `RQO` implementation will be invoked whenever the status of a request in the queue changes.

If this method is invoked and the maximum number of observers (defined by `MAX_RQOS`) has been reached, the method will exit immediately.

### add

`void RQ::add(Req *req)`

Adds the referenced request to the end of the queue.

This method will exit immediately if any of the following misuses are detected:

- `*req` has a status other than `WRITE`.
- The maximum number of requests (defined by `MAX_REQS`) has been reached.

### setStatus

`void RQ::setStatus(Req *req, unsigned char status)`

Sets the status of the specified request to `status`, if the request is in the queue.

This method will exit immediately if any of the following misuses are detected:

- `WRITE` is passed as the `status` parameter.
- `CANCELLED` is passed as the `status` parameter while the status of the request is `ACTIVE`.

`void RQ::setStatus(unsigned char rID, unsigned char status)`

Does the same as the aforementioned overloading, but with the `Request` instance given as an ID.

### get

`Req * RQ::get()`

Returns a reference to the head of the queue, or `0` if the queue is empty.

`Req * RQ::get(unsigned char rID)`

Returns a reference to the request in the queue with ID `rID`, or `0` if the request does not exist.