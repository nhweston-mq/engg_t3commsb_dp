#include "pch.h"

#include "../ENGG_T3CommsB_DP/RQ.h"

TEST(RQTestsSimple, constructor) {
    RQ *rq = new RQ();
    EXPECT_EQ(0, rq->get());
    EXPECT_EQ(0, rq->get(0));
}

TEST(RQTestsSimple, add_one_request) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    q->add(r1);
}

TEST(RQTestsSimple, get_by_id) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    q->add(r1);
    unsigned short i1 = r1->getID();
    EXPECT_EQ(r1, q->get(i1));
    EXPECT_EQ(i1, q->get(i1)->getID());
}

TEST(RQTestsSimple, get_head) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    unsigned short i1 = r1->getID();
    q->add(r1);
    EXPECT_EQ(r1, q->get());
    EXPECT_EQ(i1, q->get()->getID());
}

TEST(RQTestsSimple, active_status_set) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    q->add(r1);
    EXPECT_EQ(ACTIVE, r1->getStatus());
}

TEST(RQTestsSimple, set_status_by_pointer) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    q->add(r1);
    q->setStatus(r1, SUCCESS);
    EXPECT_EQ(SUCCESS, r1->getStatus());
}

TEST(RQTestsSimple, set_status_by_id) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    unsigned short i1 = r1->getID();
    q->add(r1);
    q->setStatus(i1, SUCCESS);
    EXPECT_EQ(SUCCESS, r1->getStatus());
}

TEST(RQTestsSimple, terminal_status_removed) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    unsigned short i1 = r1->getID();
    q->add(r1);
    q->setStatus(r1, SUCCESS);
    EXPECT_EQ(0, q->get());
    EXPECT_EQ(0, q->get(i1));
}

TEST(RQTestsMany, success) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    Req *r2 = new Req();
    Req *r3 = new Req();
    unsigned short i1, i2, i3;
    i1 = r1->getID();
    i2 = r2->getID();
    i3 = r3->getID();
    // Add all.
    q->add(r1);
    q->add(r2);
    q->add(r3);
    EXPECT_EQ(r1, q->get());
    EXPECT_EQ(r1, q->get(i1));
    EXPECT_EQ(r2, q->get(i2));
    EXPECT_EQ(r3, q->get(i3));
    EXPECT_EQ(ACTIVE, r1->getStatus());
    EXPECT_EQ(WAIT, r2->getStatus());
    EXPECT_EQ(WAIT, r3->getStatus());
    // Mark first as success.
    q->setStatus(r1, SUCCESS);
    EXPECT_EQ(r2, q->get());
    EXPECT_EQ(0, q->get(i1));
    EXPECT_EQ(r2, q->get(i2));
    EXPECT_EQ(r3, q->get(i3));
    EXPECT_EQ(SUCCESS, r1->getStatus());
    EXPECT_EQ(ACTIVE, r2->getStatus());
    EXPECT_EQ(WAIT, r3->getStatus());
    // Mark second as success.
    q->setStatus(r2, SUCCESS);
    EXPECT_EQ(r3, q->get());
    EXPECT_EQ(0, q->get(i1));
    EXPECT_EQ(0, q->get(i2));
    EXPECT_EQ(r3, q->get(i3));
    EXPECT_EQ(SUCCESS, r1->getStatus());
    EXPECT_EQ(SUCCESS, r2->getStatus());
    EXPECT_EQ(ACTIVE, r3->getStatus());
    // Mark third as success.
    q->setStatus(r3, SUCCESS);
    EXPECT_EQ(0, q->get());
    EXPECT_EQ(0, q->get(i1));
    EXPECT_EQ(0, q->get(i2));
    EXPECT_EQ(0, q->get(i3));
    EXPECT_EQ(SUCCESS, r1->getStatus());
    EXPECT_EQ(SUCCESS, r2->getStatus());
    EXPECT_EQ(SUCCESS, r3->getStatus());
}

TEST(RQTestsMany, cancellation) {
    RQ *q = new RQ();
    Req *r1 = new Req();
    Req *r2 = new Req();
    Req *r3 = new Req();
    unsigned short i1, i2, i3;
    i1 = r1->getID();
    i2 = r2->getID();
    i3 = r3->getID();
    // Add all to queue.
    q->add(r1);
    q->add(r2);
    q->add(r3);
    // Cancel second.
    q->setStatus(r2, CANCELLED);
    EXPECT_EQ(r1, q->get());
    EXPECT_EQ(r1, q->get(i1));
    EXPECT_EQ(0, q->get(i2));
    EXPECT_EQ(r3, q->get(i3));
    EXPECT_EQ(ACTIVE, r1->getStatus());
    EXPECT_EQ(CANCELLED, r2->getStatus());
    EXPECT_EQ(WAIT, r3->getStatus());
    // Mark first as success.
    q->setStatus(r1, SUCCESS);
    EXPECT_EQ(r3, q->get());
    EXPECT_EQ(0, q->get(i1));
    EXPECT_EQ(0, q->get(i2));
    EXPECT_EQ(r3, q->get(i3));
    EXPECT_EQ(SUCCESS, r1->getStatus());
    EXPECT_EQ(CANCELLED, r2->getStatus());
    EXPECT_EQ(ACTIVE, r3->getStatus());
    // Mark third as success.
    q->setStatus(r3, SUCCESS);
    EXPECT_EQ(0, q->get());
    EXPECT_EQ(0, q->get(i1));
    EXPECT_EQ(0, q->get(i2));
    EXPECT_EQ(0, q->get(i3));
    EXPECT_EQ(SUCCESS, r1->getStatus());
    EXPECT_EQ(CANCELLED, r2->getStatus());
    EXPECT_EQ(SUCCESS, r3->getStatus());
}

TEST(RQTestsMany, observers) {
    class RQOTest : public RQO {
        public:
            RQOTest() {}
            unsigned short iActive, iSuccess, iCancelled;
            void notify(Req *req) {
                switch (req->getStatus()) {
                    case ACTIVE:        iActive = req->getID();     break;
                    case SUCCESS:       iSuccess = req->getID();    break;
                    case CANCELLED:     iCancelled = req->getID();  break;
                    default: break;
                }
            }
    };
    RQOTest *ot1 = new RQOTest();
    RQOTest *ot2 = new RQOTest();
    RQO *o1 = ot1;
    RQO *o2 = ot2;
    RQ *q = new RQ();
    q->subscribe(o1);
    q->subscribe(o2);
    Req *r1 = new Req();
    Req *r2 = new Req();
    Req *r3 = new Req();
    unsigned short i1, i2, i3;
    i1 = r1->getID();
    i2 = r2->getID();
    i3 = r3->getID();
    q->add(r1);
    q->add(r2);
    q->add(r3);
    EXPECT_EQ(i1, ot1->iActive);
    EXPECT_EQ(i1, ot2->iActive);
    q->setStatus(r2, CANCELLED);
    EXPECT_EQ(i2, ot1->iCancelled);
    EXPECT_EQ(i2, ot2->iCancelled);
    q->setStatus(r1, SUCCESS);
    EXPECT_EQ(i1, ot1->iSuccess);
    EXPECT_EQ(i1, ot2->iSuccess);
    EXPECT_EQ(i3, ot1->iActive);
    EXPECT_EQ(i3, ot2->iActive);
}

TEST(RQTestsEdge, request_not_found) {
    RQ *q = new RQ();
    Req *r0 = new Req();
    Req *r1 = new Req();
    Req *r2 = new Req();
    Req *r3 = new Req();
    q->add(r1);
    q->add(r2);
    q->add(r3);
    unsigned short i0 = r0->getID();
    unsigned short i1, i2, i3;
    i1 = r1->getID();
    i2 = r2->getID();
    i3 = r3->getID();
    EXPECT_EQ(0, q->get(i0));
    q->setStatus(r0, SUCCESS);
    EXPECT_EQ(i1, q->get()->getID());
    EXPECT_EQ(i1, q->get(i1)->getID());
    EXPECT_EQ(i2, q->get(i2)->getID());
    EXPECT_EQ(i3, q->get(i3)->getID());
    EXPECT_EQ(ACTIVE, r1->getStatus());
    EXPECT_EQ(WAIT, r2->getStatus());
    EXPECT_EQ(WAIT, r3->getStatus());
    q->setStatus(i0, SUCCESS);
    EXPECT_EQ(i1, q->get()->getID());
    EXPECT_EQ(i1, q->get(i1)->getID());
    EXPECT_EQ(i2, q->get(i2)->getID());
    EXPECT_EQ(i3, q->get(i3)->getID());
    EXPECT_EQ(ACTIVE, r1->getStatus());
    EXPECT_EQ(WAIT, r2->getStatus());
    EXPECT_EQ(WAIT, r3->getStatus());
}

TEST(RQTestsEdge, too_many_requests) {
    RQ *q = new RQ();
    for (int i=0; i<MAX_REQS; i++) {
        q->add(new Req());
    }
    Req *r = new Req();
    unsigned short i = r->getID();
    q->add(r);
    EXPECT_EQ(0, q->get(i));
}

TEST(RQTestsEdge, poll_full_list_then_insert) {
    RQ *q = new RQ();
    for (int i=0; i < MAX_REQS; i++) {
        q->add(new Req());
    }
    Req *r = new Req();
    unsigned short i = r->getID();
    q->setStatus(q->get(), SUCCESS);
    q->add(r);
    EXPECT_EQ(r, q->get(i));
}

TEST(RQTestsEdge, no_cancellation_of_active) {
    RQ *q = new RQ();
    Req *r = new Req();
    unsigned short i = r->getID();
    q->add(r);
    q->setStatus(r, CANCELLED);
    EXPECT_EQ(ACTIVE, r->getStatus());
}