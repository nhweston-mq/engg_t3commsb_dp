#include "pch.h"

#include "../ENGG_T3CommsB_DP/Req.h"

TEST(ReqTests, constructor) {
    Req *req = new Req();
    EXPECT_EQ(WRITE, req->getStatus());
    EXPECT_EQ(0, req->getQuant(RED));
    EXPECT_EQ(0, req->getQuant(GREEN));
    EXPECT_EQ(0, req->getQuant(BLUE));
}

TEST(ReqTests, id_serial) {
    Req *r0 = new Req();
    unsigned short base = r0->getID();
    Req *r1 = new Req();
    Req *r2 = new Req();
    Req *r3 = new Req();
    EXPECT_EQ(base+1, r1->getID());
    EXPECT_EQ(base+2, r2->getID());
    EXPECT_EQ(base+3, r3->getID());
}

TEST(ReqTests, status_setter) {
    Req *req = new Req();
    req->setStatus(WAIT);
    EXPECT_EQ(WAIT, req->getStatus());
}

TEST(ReqTests, quant_setter) {
    Req *req = new Req();
    req->setQuant(RED, 4);
    req->setQuant(GREEN, 3);
    req->setQuant(BLUE, 2);
    EXPECT_EQ(4, req->getQuant(RED));
    EXPECT_EQ(3, req->getQuant(GREEN));
    EXPECT_EQ(2, req->getQuant(BLUE));
}

TEST(ReqTests, quant_setter_invalid_pid) {
    Req *req = new Req();
    req->setQuant(GREEN, 5);
    req->setQuant(BLUE, 4);
    req->setQuant(3, 3);
    req->setQuant(4, 2);
    EXPECT_EQ(0, req->getQuant(RED));
    EXPECT_EQ(5, req->getQuant(GREEN));
    EXPECT_EQ(4, req->getQuant(BLUE));
    EXPECT_EQ(0, req->getQuant(3));
    EXPECT_EQ(0, req->getQuant(4));
    EXPECT_EQ(0, req->getQuant(5));
}

TEST(ReqTests, quant_setter_readonly) {
    Req *req = new Req();
    req->setQuant(RED, 5);
    req->setQuant(GREEN, 4);
    req->setStatus(WAIT);
    req->setQuant(GREEN, 3);
    req->setQuant(BLUE, 2);
    EXPECT_EQ(5, req->getQuant(RED));
    EXPECT_EQ(4, req->getQuant(GREEN));
    EXPECT_EQ(0, req->getQuant(BLUE));
}